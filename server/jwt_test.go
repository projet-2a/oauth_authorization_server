package server

import (
	"fmt"
	"testing"
	"time"
)

func TestCreateToken(t *testing.T) {

	in, _ := time.ParseDuration("10s")

	s := CreateToken("key", in, "auth", "client", "id", "noscope")

	fmt.Printf("Token: %s", s)
	// if status := rr.Code; status != http.StatusOK {
	// 	t.Errorf("handler returned wrong status code: got %v want %v",
	// 		status, http.StatusOK)
	// }

	// expected := `Hello world!`
	// if rr.Body.String() != expected {
	// 	t.Errorf("handler returned unexpected body: got %v want %v",
	// 		rr.Body.String(), expected)
	// }
}
