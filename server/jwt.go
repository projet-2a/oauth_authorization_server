package server

import (
	"fmt"
	"log"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	uuid "github.com/satori/go.uuid"
)

// AuthorizationClaims contains all custom field for oauth token included in the jwt
type AuthorizationClaims struct {
	ClientID string `json:"cid"`
	Scope    string `json:"scope"`
	jwt.StandardClaims
}

// ValidateToken verify and decode a JWT token
func ValidateToken(tokenString string, key string) (*AuthorizationClaims, error) {
	// Parse takes the token string and a function for looking up the key. The latter is especially
	// useful if you use multiple keys for your application.  The standard is to use 'kid' in the
	// head of the token to identify which key to use, but the parsed token (head and claims) is provided
	// to the callback, providing flexibility.
	token, err := jwt.ParseWithClaims(tokenString, &AuthorizationClaims{}, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		hmacSampleSecret := []byte(key)
		return hmacSampleSecret, nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(*AuthorizationClaims); ok && token.Valid {
		return claims, nil
		// } else if ve, ok := err.(*jwt.ValidationError); ok {
		// 	if ve.Errors&jwt.ValidationErrorMalformed != 0 {
		// 		fmt.Println("That's not even a token")
		// 	} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
		// 		// Token is either expired or not active yet
		// 		fmt.Println("Timing is everything")
		// 	} else {
		// 		fmt.Println("Couldn't handle this token:", ve.Inner)
		// 	}
		// } else {
		// 	fmt.Println("Couldn't handle this token")
	}
	return nil, err
}

// CreateToken create and sign a JWT token
func CreateToken(secretKey string, expireIn time.Duration, serverURL string, subject string, clientID string, scope string) string {
	now := time.Now()

	expireAt := now.Add(expireIn)

	// Create the Claims
	claims := &AuthorizationClaims{
		ClientID: clientID,
		Scope:    scope,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireAt.Unix(),
			Issuer:    serverURL,
			IssuedAt:  now.Unix(),
			Subject:   subject,
			// NotBefore: now,
			// Audience:  "test",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(secretKey))
	// fmt.Printf("%v %v", ss, err)

	if err != nil {
		log.Panic(err)
	}

	return ss
}

// CreateCode create and sign a JWT code
func CreateCode(secretKey string, expireIn time.Duration, subject string, clientID string, redirectURI string, scope string) string {
	now := time.Now()

	expireAt := now.Add(expireIn)

	// Create the Claims
	claims := &AuthorizationClaims{
		ClientID: clientID,
		Scope:    scope,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireAt.Unix(),
			IssuedAt:  now.Unix(),
			Subject:   subject,
			Audience:  redirectURI,
			Id:        uuid.Must(uuid.NewV4()).String(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString([]byte(secretKey))

	if err != nil {
		log.Panic(err)
	}

	return ss
}
