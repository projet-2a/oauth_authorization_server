package server

// AuthorizeCodeResponse is the response structure for Authorize endpoint in Code Grant
type AuthorizeCodeResponse struct {
	Code  string `url:"code,omitempty"`
	State string `url:"state,omitempty"`
}
