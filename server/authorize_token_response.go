package server

// AuthorizeTokenResponse is the response structure for Authorize endpoint in Implicit Grant
type AuthorizeTokenResponse struct {
	AccessToken string    `url:"access_token,omitempty"`
	TokenType   TokenType `url:"token_type,omitempty"`
	ExpiresIn   int       `url:"expires_in,omitempty"`
	Scope       string    `url:"scope,omitempty"`
	State       string    `url:"state,omitempty"`
}
