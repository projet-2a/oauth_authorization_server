package server

// MimeType is the oauth query parameters encoding
type MimeType string

// All availables MimeType for this server
const (
	MimeJSON        MimeType = "application/json"
	MimeFormEncoded MimeType = "application/x-www-form-urlencoded"
)
