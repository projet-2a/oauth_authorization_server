package server

// HintType is a hint about the type of the token submitted for revocation or introspection
type HintType string

// List of HintType
const (
	AccessToken  HintType = "access_token"
	RefreshToken HintType = "refresh_token"
)
