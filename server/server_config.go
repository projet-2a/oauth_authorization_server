package server

// Scope is a key to access specific information
type Scope struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

// Config is all the global variable for the server
type Config struct {
	DB        string
	AppName   string
	ServerURL string
	Scopes    []Scope
	Endpoints map[string]string
	SecretKey string
}
