package dao

import (
	"errors"

	"gopkg.in/mgo.v2/bson"
)

const applicationsCollection = "apps"

// Application model
type Application struct {
	ID           bson.ObjectId `bson:"_id"`
	ClientID     string        `bson:"client_id"`
	ClientSecret string        `bson:"client_secret"`
	RedirectURI  string        `bson:"redirect_uri"`
}

// AppDAO is the struct used to manipulate applications
type AppDAO struct{}

// FindAll the list of all applications
func (m *AppDAO) FindAll() ([]Application, error) {
	var apps []Application
	err := db.C(applicationsCollection).Find(bson.M{}).All(&apps)
	return apps, err
}

// FindByID find a app by its id
func (m *AppDAO) FindByID(id bson.ObjectId) (Application, error) {
	var app Application
	err := db.C(applicationsCollection).FindId(id).One(&app)
	return app, err
}

// FindByClientID find a app by its client_id
func (m *AppDAO) FindByClientID(clientID string) (Application, error) {
	var app Application
	err := db.C(applicationsCollection).Find(bson.M{"client_id": clientID}).One(&app)
	return app, err
}

// CheckClientSecret check the client_secret of an app
func (m *AppDAO) CheckClientSecret(clientID string, clientSecret string) bool {
	validApp := false
	app, err := m.FindByClientID(clientID)
	if err == nil {
		validApp = clientSecret == app.ClientSecret
	}
	return validApp
}

// Insert allow to insert a app into database
func (m *AppDAO) Insert(app Application) error {
	_, err := m.FindByClientID(app.ClientID)
	if err != nil {
		app.ID = bson.NewObjectId()
		err = db.C(applicationsCollection).Insert(&app)
	} else {
		err = errors.New("Client already exist")
	}
	return err
}

// Delete allow to delete an existing app
func (m *AppDAO) Delete(app Application) error {
	err := db.C(applicationsCollection).Remove(&app)
	return err
}

// Update allow to update an existing app
func (m *AppDAO) Update(app Application) error {
	err := db.C(applicationsCollection).UpdateId(app.ID, &app)
	return err
}
