package server

import (
	"fmt"
	"strings"
)

//ParseScope keep only server scope from request scope and remove duplicated
func ParseScope(serverScopes []Scope, requestScope string) string {
	rawScopes := strings.Split(requestScope, " ")
	scopeLen := len(rawScopes)
	duplicatedScopes := make([]string, 0, scopeLen)
	scopes := make([]Scope, 0, scopeLen)

	for _, scopeName := range rawScopes {
		duplicate := false
		for _, name := range duplicatedScopes {
			if name == scopeName {
				duplicate = true
				break
			}
		}
		if !duplicate {
			duplicatedScopes = append(duplicatedScopes, scopeName)
		}
	}

	for _, scopeName := range duplicatedScopes {
		for _, sc := range serverScopes {
			if sc.Name == scopeName {
				scopes = append(scopes, sc)
				break
			}
		}
	}

	ret := strings.Join(duplicatedScopes, " ")
	return ret
}

//ValidateScope check if the request scope contains only server scope ans there are no duplicate
func ValidateScope(serverScopes []Scope, requestScope string) ([]Scope, error) {

	if requestScope == "" {
		return make([]Scope, 0, 0), nil
	}

	scopesList := strings.Split(requestScope, " ")
	scopeLen := len(scopesList)
	scopes := make([]Scope, 0, scopeLen)

	for _, scopeName := range scopesList {
		duplicate := 0
		for _, name := range scopesList {
			if name == scopeName {
				duplicate++
			}
		}
		if duplicate != 1 {
			return nil, fmt.Errorf("Duplicated scope: %s", scopeName)
		}
	}

	for _, scopeName := range scopesList {
		found := false
		for _, sc := range serverScopes {
			if sc.Name == scopeName {
				found = true
				scopes = append(scopes, sc)
				break
			}
		}
		if !found {
			return nil, fmt.Errorf("Scope not exist: %s", scopeName)
		}
	}

	return scopes, nil

}
