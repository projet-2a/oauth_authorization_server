# How to use this package

## Main server

### Compile and Run

```
go get
go build
./oauth_authorization_server
```

The server will run on port `8080`.

### Configuration

Change configuration file maned `config.json`.

 - scopes : is all availables scopes of data
 - endpoint : is a map of each RFC endpoint name to the URL path in the API
 - application : is the single available application with client credentials to access the token

## Tools

### Keygen

This tool generate application secure random credentials.

```
> $ ./keygen
Client ID:     5e6f964d172d47e6d86c
Client Secret: 4ecb5bc63af8c0dfda7356bbb5300c1b
```

