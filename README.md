# OAuth_authorization_server

OAuth authorization server.

- [RFC-6749](https://tools.ietf.org/html/rfc6749) Oauth2 Framework
- [RFC-6750](https://tools.ietf.org/html/rfc6750) Oauth2 Framework Bearer Token
- [RFC-7662](https://tools.ietf.org/html/rfc7662) Oauth2 Token Introspection

- [RFC-7009](https://tools.ietf.org/html/rfc7009) Oauth2 Token Revocation


## Other doc

- [OAuth 2 Simplified](https://aaronparecki.com/oauth-2-simplified/)

## Existing server example

[Good python implementation](https://docs.authlib.org/en/latest/index.html)
[Google oauth doc](https://developers.google.com/identity/protocols/OAuth2WebServer)

[Google server to server approach](https://developers.google.com/identity/protocols/OAuth2ServiceAccount)
Using ServiceAccount (grant-type is **urn:ietf:params:oauth:grant-type:jwt-bearer**)
It use [RFC-7523](https://tools.ietf.org/html/rfc7523)


## Technologies

- Golang
- MongoDB

## Client settings and permissions

Client settings and permissions used by the authorization server are defined by a JSON configuration file like this one:

```json
{
    "app_name": "TODO App",
    "redirect_uri": "http://xxx.yyy/callback",
    "scopes": [
        {
            "name": "list",
            "description": "List all your tasks."
        },
        {
            "name": "read",
            "description": "Read a task knowing it's identifier."
        },
        {
            "name": "create",
            "description": "Create a task."
        },
        {
            "name": "delete",
            "description": "Delete a task."
        },
        {
            "name": "update",
            "description": "Update a task."
        },
        {
            "name": "email",
            "description": "Get your email."
        }
    ],
    "endpoints": {
        "token": "token",
        "authorize": "authorize",
        "introspect": "introspect"
    }
}
```

# Fake DB data
[Fake Data generator](https://www.mockaroo.com/)

## Golang

Write code in GO

- https://talks.golang.org/2014/organizeio.slide#9

- https://golang.org/doc/code.html

- https://github.com/golang-standards/project-layout

## Authorization Server entry points

- Authorization Code Grant (type code)

    * Request

        ```
        GET /authorize?response_type=code&client_id=s6BhdRkqt3&state=xyz&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1
        Host: server.example.com
        ```

    * Response
        
        ```
        HTTP/1.1 302 Found
        Location: https://client.example.com/cb?code=SplxlOBeZQQYbYS6WxSbIA&state=xyz
        ```

- Access Token (type authorization_code):

    * Request

        ```
        POST /token HTTP/1.1
        Host: server.example.com
        Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
        Content-Type: application/x-www-form-urlencoded

        grant_type=authorization_code&code=SplxlOBeZQQYbYS6WxSbIA&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb
        ```

    * Response:

        ```
        HTTP/1.1 200 OK
        Content-Type: application/json;charset=UTF-8
        Cache-Control: no-store
        Pragma: no-cache

        {
            "access_token":"2YotnFZFEjr1zCsicMWpAA",
            "token_type":"example",
            "expires_in":3600,
            "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
            "example_parameter":"example_value"
        }
        ```


- Implicit Grant (type token):

    * Request

        ```
        GET /authorize?response_type=token&client_id=s6BhdRkqt3&state=xyz&redirect_uri=https%3A%2F%2Fclient%2Eexample%2Ecom%2Fcb HTTP/1.1
        Host: server.example.com
        ```

    * Response:

        ```
        HTTP/1.1 302 Found
        Location: http://example.com/cb#access_token=2YotnFZFEjr1zCsicMWpAA&state=xyz&token_type=example&expires_in=3600
        ```

- Client Credentials Grant (type client_credentials):

    * Request

        ```
        POST /token HTTP/1.1
        Host: server.example.com
        Authorization: Basic YWRtaW46cGFzc3dvcmQK
        Content-Type: application/x-www-form-urlencoded

        grant_type=client_credentials
        ```

    * Response

        ```
        HTTP/1.1 200 OK
        Content-Type: application/json;charset=UTF-8
        Cache-Control: no-store
        Pragma: no-cache

        {
            "access_token":"2YotnFZFEjr1zCsicMWpAA",
            "token_type":"example",
            "expires_in":3600,
            "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
            "example_parameter":"example_value"
        }
        ```


- Resource Owner Password Credentials Grant (type password):

    * Request

        ```
        POST /token HTTP/1.1
        Host: server.example.com
        Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
        Content-Type: application/x-www-form-urlencoded

        grant_type=password&username=johndoe&password=A3ddj3w
        ```

    * Response

        ```
        HTTP/1.1 200 OK
        Content-Type: application/json;charset=UTF-8
        Cache-Control: no-store
        Pragma: no-cache

        {
            "access_token":"2YotnFZFEjr1zCsicMWpAA",
            "token_type":"example",
            "expires_in":3600,
            "example_parameter":"example_value"
        }
        ```


- Refresh Access Token (type refresh_token):

    * Request

        ```
        POST /token HTTP/1.1
        Host: server.example.com
        Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
        Content-Type: application/x-www-form-urlencoded

        grant_type=refresh_token&refresh_token=tGzv3JOkF0XG5Qx2TlKWIA
        ```

    * Response

        ```
        HTTP/1.1 200 OK
        Content-Type: application/json;charset=UTF-8
        Cache-Control: no-store
        Pragma: no-cache

        {
            "access_token":"2YotnFZFEjr1zCsicMWpAA",
            "token_type":"example",
            "expires_in":3600,
            "refresh_token":"tGzv3JOkF0XG5Qx2TlKWIA",
            "example_parameter":"example_value"
        }
        ```

- Token Introspection (from RFC-7662):

    * Request

        ```
        POST /introspect HTTP/1.1
        Host: server.example.com
        Accept: application/json
        Content-Type: application/x-www-form-urlencoded
        Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW

        token=mF_9.B5f-4.1JqM&token_type_hint=access_token
        ```

    * Response

        ```
        HTTP/1.1 200 OK
        Content-Type: application/json

        {
         "active": true,
         "client_id": "l238j323ds-23ij4",
         "username": "jdoe",
         "scope": "read write dolphin",
         "sub": "Z5O3upPC88QrAjx00dis",
         "aud": "https://protected.example.net/resource",
         "iss": "https://server.example.com/",
         "exp": 1419356238,
         "iat": 1419350238,
         "extension_field": "twenty-seven"
        }
        ```    

- Token Revocation (from rfc-7009):

    * Request

        ```
        POST /revoke HTTP/1.1
        Host: server.example.com
        Content-Type: application/x-www-form-urlencoded
        Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
   
        token=45ghiukldjahdnhzdauz&token_type_hint=refresh_token
        ```
    * Response

        ```
        HTTP/1.1 OK
        ```
        On error: {"error":"unsupported_token_type"}


## Potential programming mistake 

Go Request object's Form attribute contain body and url parameters, use PostForm attribute instead.
For oauth token endpoint query parameters MUST be in body not in url.

Post parameters in the request can be passed by multiple way, the recommended is x-www-form-urlencoded.
Json can be also used.
Server MUST check media type in Content-Type header.


