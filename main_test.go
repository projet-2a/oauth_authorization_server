package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"./server"
	"./server/dao"
)

const (
	SecretKey = "server_tests"
)

var app dao.Application

func init() {
	dao.Connect("localhost:27017", "auth_test")
	app = dao.Application{
		ClientID:     "username",
		ClientSecret: "password",
		RedirectURI:  "https://foo.bar",
	}
	appDao := dao.AppDAO{}
	appDao.Insert(app)
}

func testRequest(req *http.Request) *httptest.ResponseRecorder {
	config := &server.Config{
		AppName:   "TestApp",
		ServerURL: "https://test.com",
		SecretKey: SecretKey,
		Scopes:    nil,
		DB:        "",
		Endpoints: map[string]string{
			"token":      "token",
			"authorize":  "authorize",
			"introspect": "introspect",
		},
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := server.NewRouter(*config)

	handler.ServeHTTP(rr, req)

	return rr
}

func TestIndex(t *testing.T) {
	req, err := http.NewRequest("GET", "/v1/", nil)
	// req, err := http.NewRequest("GET", "/v1/", strings.NewReader("salut"))
	if err != nil {
		t.Fatal(err)
	}

	config := &server.Config{
		AppName: "TestApp",
		Scopes:  nil,
		DB:      "",
		Endpoints: map[string]string{
			"token":      "token",
			"authorize":  "authorize",
			"introspect": "introspect",
		},
	}

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := server.NewRouter(*config)

	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := `Hello World!`
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestInvalidGrantType(t *testing.T) {
	data := `{"grant_type":"invalid"}`

	req, err := http.NewRequest("POST", "/v1/token", strings.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.ErrorToken
	err = decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if res.ErrorName != server.UnsupportedGrantType {
		t.Errorf("handler returned unexpected ErrorName: got %v want %v", res.ErrorName, server.InvalidGrant)
	}
	if res.State != "" {
		t.Errorf("error state must be empty")
	}
}

func TestClientCredentialsJson(t *testing.T) {
	scope := "email username"
	data := `{"grant_type":"client_credentials", "scope":"email username"}`

	req, _ := http.NewRequest("POST", "/v1/token", strings.NewReader(data))
	req.SetBasicAuth("username", "password")
	req.Header.Set("Content-Type", "application/json")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.TokenResponse
	err := decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if res.TokenType != server.BEARER {
		t.Errorf("handler returned unexpected TokenType: got %v want %v", res.TokenType, server.BEARER)
	}
	if res.ExpiresIn == 0 {
		t.Errorf("handler returned unexpected ExpiresIn equal to zero")
	}
	if res.Scope != scope {
		t.Errorf("handler returned unexpected TokenType: got %s want %s", res.Scope, scope)
	}
}
func TestClientCredentialsForm(t *testing.T) {
	scope := "email username"
	data := `grant_type=client_credentials&scope=email username`

	req, _ := http.NewRequest("POST", "/v1/token", strings.NewReader(data))
	req.SetBasicAuth("username", "password")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.TokenResponse
	err := decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if res.TokenType != server.BEARER {
		t.Errorf("handler returned unexpected TokenType: got %v want %v", res.TokenType, server.BEARER)
	}
	if res.ExpiresIn == 0 {
		t.Errorf("handler returned unexpected ExpiresIn equal to zero")
	}
	if res.Scope != scope {
		t.Errorf("handler returned unexpected TokenType: got %s want %s", res.Scope, scope)
	}
}

func TestResourcesOwnerPassword(t *testing.T) {
	t.Skipf("Not Implemented")

	scope := "email username"
	data := `{"grant_type":"password", "scope":"email username", "username":"username", "password": "password"}`

	req, _ := http.NewRequest("POST", "/v1/token", strings.NewReader(data))
	req.Header.Set("Content-Type", "application/json")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	fmt.Println(rr.Body)

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.TokenResponse
	err := decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if res.TokenType != server.BEARER {
		t.Errorf("handler returned unexpected TokenType: got %v want %v", res.TokenType, server.BEARER)
	}
	if res.ExpiresIn == 0 {
		t.Errorf("handler returned unexpected ExpiresIn equal to zero")
	}
	if res.Scope != scope {
		t.Errorf("handler returned unexpected TokenType: got %s want %s", res.Scope, scope)
	}
}

func TestTokenIntrospectionWrong(t *testing.T) {
	// token := "fuyfeiufheuzfgugheriugiuerzeuhiezhg"
	data := `{"token":"fuyfeiufheuzfgugheriugiuerzeuhiezhg"}`

	req, err := http.NewRequest("POST", "/v1/introspect", strings.NewReader(data))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth("username", "password")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.Introspect
	err = decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if res.Active {
		t.Errorf("handler returned unexpected Active")
	}
}

func TestTokenIntrospectionExpire(t *testing.T) {
	d, _ := time.ParseDuration("-1m")
	token := server.CreateToken(SecretKey, d, "", "", "", "")

	data := fmt.Sprintf("token=%s", token)

	req, err := http.NewRequest("POST", "/v1/introspect", strings.NewReader(data))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth("username", "password")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.Introspect
	err = decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if res.Active {
		t.Errorf("handler returned unexpected Active")
	}
}

func TestTokenIntrospectionOK(t *testing.T) {
	d, _ := time.ParseDuration("1h")
	token := server.CreateToken(SecretKey, d, "", "", "", "")
	fmt.Println(token)

	data := fmt.Sprintf("token=%s", token)

	req, err := http.NewRequest("POST", "/v1/introspect", strings.NewReader(data))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth("username", "password")

	rr := testRequest(req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check content type
	if content := rr.Header().Get("Content-Type"); content != "application/json; charset=UTF-8" {
		t.Errorf("handler returned wrong content type: got %v want %v",
			content, "application/json; charset=UTF-8")
	}

	// Check the response body is what we expect.
	decoder := json.NewDecoder(rr.Body)
	var res server.Introspect
	err = decoder.Decode(&res)
	if err != nil {
		t.Fatal(err)
	}
	if !res.Active {
		t.Errorf("handler returned unexpected not Active")
	}
}
