package config

import (
	"encoding/json"
	"os"
)

type Scope struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

type ClientConfiguration struct {
	AppName     string            `json:"app_name"`
	ServerURL   string            `json:"server_url"`
	Scopes      []Scope           `json:"scopes"`
	Endpoints   map[string]string `json:"endpoints"`
	Application Application       `json:"application"`
}

type Application struct {
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	RedirectURI  string `json:"redirect_uri"`
}

func LoadClientConfig(filename string) (ClientConfiguration, error) {
	// Configuration struct
	config := ClientConfiguration{}
	// Open the JSON config file
	jsonFile, err := os.Open(filename)
	// Handle os.Open errors
	if err != nil {
		return config, err
	}
	// Defer the closing of the JSON file
	defer jsonFile.Close()
	// Parse the file
	decoder := json.NewDecoder(jsonFile)
	err = decoder.Decode(&config)
	// Handle decode error
	if err != nil {
		return config, err
	}
	// Return statement
	return config, nil
}
