package config

import (
	"fmt"
	"testing"
)

func TestLoadClientConfig(t *testing.T) {
	config, err := LoadClientConfig("test-config.json")
	// Handles errors
	if err != nil {
		t.Fatal(err)
	}
	// Output
	if "TODO App" != config.AppName {
		t.Errorf("app name not match, must be App name not %s", config.AppName)
	}

	if "https://auth.example.com" != config.ServerURL {
		t.Errorf("server URL not match, must be https://auth.example.com not %s", config.ServerURL)
	}

	fmt.Println("Scopes:")
	for _, scope := range config.Scopes {
		fmt.Println("- Name: " + scope.Name)
		fmt.Println("  Description: " + scope.Description)
	}

	fmt.Println("Endpoints:")
	for name, url := range config.Endpoints {
		fmt.Printf("- %s: %s\n", name, url)
	}

	app := config.Application
	if "http://xxx.yyy/callback" != app.RedirectURI {
		t.Errorf("redirect uri not match, must be http://xxx.yyy/callback not %s", app.RedirectURI)
	}
}
