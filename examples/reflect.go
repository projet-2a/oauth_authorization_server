package main

import (
	"fmt"
	"reflect"
)

func main() {
	type S struct {
		F0 string `alias:"field_0"`
		F1 string `alias:""`
		F2 string 
	}

	var s S

	val := reflect.ValueOf(&s).Elem()
    st := val.Type()
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
        ft := st.Field(i)
		if alias, ok := ft.Tag.Lookup("alias"); ok {
			if alias == "" {
				fmt.Println("(blank)")
				field.SetString("(blank)")
			} else {
				fmt.Println(alias)
				field.SetString(alias)
			}
		} else {
			fmt.Println("(not specified)")
			field.SetString("(not specified)")
		}
	}
//    d := val.Addr().Interface()
	fmt.Printf("%+v\n", s)

}
