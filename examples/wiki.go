// Copyright 2010 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/mux"
)

type Page struct {
	Title string
	Body  []byte
}

type FileList struct {
	WorkingDirectory string
	Filenames        []string
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func viewlistHandler(w http.ResponseWriter, r *http.Request) {
	workingDirectory, _ := filepath.Abs(".")
	var files []string
	err := filepath.Walk(workingDirectory, func(path string, info os.FileInfo, err error) error {
		var extension = filepath.Ext(path)
		if extension == ".txt" {
			var filename = path[0 : len(path)-len(extension)]
			files = append(files, filepath.Base(filename))
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	fileList := &FileList{
		WorkingDirectory: workingDirectory,
		Filenames:        files,
	}
	tmpl, _ := template.ParseFiles("viewList.html")
	tmpl.Execute(w, fileList)
}

func viewHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	p, err := loadPage(params["title"])
	if err != nil {
		http.Redirect(w, r, "/edit/"+params["title"], http.StatusFound)
		return
	}
	renderTemplate(w, "view", p)
}

func editHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	p, err := loadPage(params["title"])
	if err != nil {
		p = &Page{Title: params["title"]}
	}
	renderTemplate(w, "edit", p)
}

func saveHandler(w http.ResponseWriter, r *http.Request) {
	body := r.FormValue("body")
	params := mux.Vars(r)

	p := &Page{Title: params["title"], Body: []byte(body)}
	err := p.save()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/view/"+params["title"], http.StatusFound)
}

var templates = template.Must(template.ParseFiles("edit.html", "view.html"))

func renderTemplate(w http.ResponseWriter, tmpl string, p *Page) {
	err := templates.ExecuteTemplate(w, tmpl+".html", p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func CreateHander() http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/view/", viewlistHandler).Methods("GET")
	r.HandleFunc("/view/{title}", viewHandler).Methods("GET")
	r.HandleFunc("/edit/{title}", editHandler).Methods("GET")
	r.HandleFunc("/save/{title}", saveHandler).Methods("POST")

	return r
}

func main() {
	r := CreateHander()
	log.Fatal(http.ListenAndServe(":8080", r))
}
