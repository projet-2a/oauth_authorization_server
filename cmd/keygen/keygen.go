package main

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
)

func keygen(c int) string {
	keystream := make([]byte, c)
	_, err := rand.Read(keystream)
	if err != nil {
		panic(err)
	}

	key := make([]byte, c*2)
	hex.Encode(key, keystream)
	return string(key)
}

func main() {
	client := keygen(10)
	secret := keygen(16)

	fmt.Printf("Client ID:     %s\nClient Secret: %s\n", client, secret)
}
