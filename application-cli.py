#!/usr/bin/python3
# coding: utf-8
import argparse
from enum import Enum

class Color(Enum):
    RED = '\033[31m'
    GRN = '\033[32m'
    RST = '\033[0m'

try:
    from pymongo import MongoClient
except ImportError as e:
    print("\033[31mpymongo is required and not installed, use pip install pymongo\033[0m")
    print(e)
    exit(1)

def parse_args():
    """Arguments parsing."""
    parser = argparse.ArgumentParser(description='Automatic user edition')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--add',
                       action='store_true',
                       help='Add application')
    group.add_argument('--remove',
                       action='store_true',
                       help='Remove application')
    group.add_argument('--list',
                       action='store_true',
                       help='List all apps')

    parser.add_argument('--mongo-host',
                        type=str,
                        default='localhost',
                        help='Mongo host, default: localhost',
                        )

    parser.add_argument('--mongo-port',
                        type=int,
                        default=27017,
                        help='Mongo port, default: 27017',
                        )

    parser.add_argument('-i',
                        '--id',
                        type=str,
                        help='Client ID',
                        )

    parser.add_argument('-s',
                        '--secret',
                        type=str,
                        help='Client Secret',
                        )

    parser.add_argument('-r',
                        '--redirect',
                        type=str,
                        help='Client redirect uri',
                        )

    args = parser.parse_args()

    return args


if __name__ == '__main__':

    parser = parse_args()

    print(parser)

    mongo = MongoClient(parser.mongo_host, parser.mongo_port)
    db = mongo.auth.apps

    if parser.list:
        cursor = db.find()
        print('[*] Apps:')
        for user in cursor:
            print(' - %s, %s, %s' % (user['client_id'], user['client_secret'], user['redirect_uri']))

    else:
        if not parser.id:
            raise Exception('No id')
        if not parser.secret:
            raise Exception('No secret')
        if not parser.redirect:
            raise Exception('No redirect')

        if parser.add:
            # Check user not exist
            if db.find({'client_id': parser.id}).count() != 0:
                raise Exception('App already exist')

            db.insert({
                'client_id': parser.id,
                'client_secret': parser.secret,
                'redirect_uri': parser.redirect,
            })

        elif parser.remove:
            confirm = input('Remove %s [y/N]: ' % parser.id).lower()
            if confirm == 'y':
                ret = db.remove({'client_id': parser.id}, {'justOne': True})

                print(ret)
